<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240402095942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE markers (id INT AUTO_INCREMENT NOT NULL, questions_id INT NOT NULL, quete_id INT DEFAULT NULL, lat DOUBLE PRECISION NOT NULL, lon DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_4189DF30BCB134CE (questions_id), INDEX IDX_4189DF307A1B721E (quete_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, texte VARCHAR(255) NOT NULL, reponse1 VARCHAR(255) NOT NULL, reponse2 VARCHAR(255) NOT NULL, reponse3 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE markers ADD CONSTRAINT FK_4189DF30BCB134CE FOREIGN KEY (questions_id) REFERENCES questions (id)');
        $this->addSql('ALTER TABLE markers ADD CONSTRAINT FK_4189DF307A1B721E FOREIGN KEY (quete_id) REFERENCES quetes (id)');
        $this->addSql('ALTER TABLE quetes ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quetes ADD CONSTRAINT FK_CE0868A1A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_CE0868A1A76ED395 ON quetes (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE quetes DROP FOREIGN KEY FK_CE0868A1A76ED395');
        $this->addSql('ALTER TABLE markers DROP FOREIGN KEY FK_4189DF30BCB134CE');
        $this->addSql('ALTER TABLE markers DROP FOREIGN KEY FK_4189DF307A1B721E');
        $this->addSql('DROP TABLE markers');
        $this->addSql('DROP TABLE questions');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP INDEX IDX_CE0868A1A76ED395 ON quetes');
        $this->addSql('ALTER TABLE quetes DROP user_id');
    }
}
