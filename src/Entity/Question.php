<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]

#[ApiResource(
    normalizationContext: ['groups' => ['question_read']],
)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    
    #[Groups(['quest_read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    
    #[Groups(['quest_read'])]
    private ?string $text = null;

    #[ORM\Column(length: 255)]
    
    #[Groups(['quest_read'])]
    private ?string $reponse1 = null;

    #[ORM\Column(length: 255)]
    
    #[Groups(['quest_read'])]
    private ?string $reponse2 = null;

    #[ORM\Column(length: 255)]
    
    #[Groups(['quest_read'])]
    private ?string $reponse3 = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): static
    {
        $this->text = $text;

        return $this;
    }

    public function getReponse1(): ?string
    {
        return $this->reponse1;
    }

    public function setReponse1(string $reponse1): static
    {
        $this->reponse1 = $reponse1;

        return $this;
    }

    public function getReponse2(): ?string
    {
        return $this->reponse2;
    }

    public function setReponse2(string $reponse2): static
    {
        $this->reponse2 = $reponse2;

        return $this;
    }

    public function getReponse3(): ?string
    {
        return $this->reponse3;
    }

    public function setReponse3(string $reponse3): static
    {
        $this->reponse3 = $reponse3;

        return $this;
    }
}
