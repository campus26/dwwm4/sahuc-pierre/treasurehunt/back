<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\MarkerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MarkerRepository::class)]
#[ApiResource]
class Marker
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['quest_read'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['quest_read'])]
    private ?float $lat = null;

    #[ORM\Column]
    #[Groups(['quest_read'])]
    private ?float $lon = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(['quest_read'])]
    private ?Question $questionsId = null;

    #[ORM\ManyToOne(inversedBy: 'markers')]
    private ?Quest $questId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): static
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function setLon(float $lon): static
    {
        $this->lon = $lon;

        return $this;
    }

    public function getQuestionsId(): ?Question
    {
        return $this->questionsId;
    }

    public function setQuestionsId(?Question $questionsId): static
    {
        $this->questionsId = $questionsId;

        return $this;
    }

    public function getQuestId(): ?Quest
    {
        return $this->questId;
    }

    public function setQuestId(?Quest $questId): static
    {
        $this->questId = $questId;

        return $this;
    }
}
