<?php

namespace App\DataFixtures;

use App\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class QuestionFixtures extends Fixture
{
    public const QUESTIONS_REFERENCE = '';

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $question = new Question();
            $question->setText('Question ' . $i);
            $question->setReponse1('Reponse 1 - ' . $i);
            $question->setReponse2('Reponse 2 - ' . $i);
            $question->setReponse3('Reponse 3 - ' . $i);
            $manager->persist($question);

            $this->addReference(self::QUESTIONS_REFERENCE.$i, $question);
        }

        $manager->flush();
    }
}
