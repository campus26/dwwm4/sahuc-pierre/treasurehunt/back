<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class UsersFixtures extends Fixture implements DependentFixtureInterface
{
     /**
     * @var UserPasswordEncoderInterface
     */
    private $hasher;
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }
    
    public function hashPassword($user, $plainPassword)
    {
        return $this->hasher->hashPassword($user, $plainPassword);
    }

    public function load(ObjectManager $manager): void
    {
        
        $admin = new User();
        $admin->setEmail('pierresahuc@gmail.com');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->hashPassword($admin, 'admin'));
        
        $user = new User();
        $user->setEmail('user@monrpojet.com');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->hashPassword($user, 'user'));

        $manager->persist($admin);
        $manager->persist($user);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            QuestFixtures::class,
        ];
    }
}
