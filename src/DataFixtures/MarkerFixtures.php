<?php

namespace App\DataFixtures;

use App\Entity\Marker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MarkerFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $QuestionsPreviouslyChoosen = [];
        for ($i = 0; $i < 10; $i++) {
            $marker = new Marker();
            $latitude = rand(-90, 90);
            $longitude = rand(-180, 180);
            $marker->setLat($latitude);
            $marker->setLon($longitude);

            // choix d'un index aleatoire d'une question parmi celles qui n'ont pas encore été utilisé
            $PossibleQuestion = array_diff(range(0, 9), $QuestionsPreviouslyChoosen);
            $randomquestion = $PossibleQuestion[array_rand($PossibleQuestion)];
            // Usage d'Object Reference pour lier une question à un marker
            $marker->setQuestionsId($this->getReference(QuestionFixtures::QUESTIONS_REFERENCE.$randomquestion));
            $manager->persist($marker);

            $QuestionsPreviouslyChoosen[] = $randomquestion;
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            QuestionFixtures::class,
        ];
    }
}
