<?php

namespace App\DataFixtures;

use App\Entity\Quest;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class QuestFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 4; $i++) {
            $quete = new Quest();
            $quete->setName('Quete ' . $i);
            $quete->setCreation(new \DateTimeImmutable());
            $quete->setDuration(3600*$i);

            $previouslyChoosenMarkers = [];
            for ($j = 0; $j < mt_rand(2, 9); $j++) {
                $PossibleMarkers = array_diff(range(0, 9), $previouslyChoosenMarkers);
                $randomMarker = $PossibleMarkers[array_rand($PossibleMarkers)];

                $previouslyChoosenMarkers[] = $randomMarker;
            }
            $manager->persist($quete);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MarkerFixtures::class,
        ];
    }
}
